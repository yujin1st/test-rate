<?php
/**
 * User: yujin
 * Date: 30.03.16
 */

/**
 * Class for import data from remote and local files
 *
 */
class Import
{
  /** source of data */
  const SOURCE_WEB = 1;
  const SOURCE_LOCAL = 2;

  /** domain source */
  public $domain = 'http://test-rates.dev';

  private $_db;

  /** @var bool testing mode */
  public $testing = false;

  /**
   * @return mixed
   */
  public function getDb() {
    if (!$this->_db) $this->_db = $this->connect();
    return $this->_db;
  }

  /**
   * connecting to db
   *
   * @return mysqli
   */
  private function connect() {
    return $this->db = mysqli_connect(
      '127.0.0.1',
      'root',
      'q',
      'test-rates');
  }

  /**
   * Updating record by symbol
   *
   * @param $code string currency code
   * @param $rate double currency rate
   */
  public function update($code, $rate) {
    $exist = mysqli_query($this->getDb(), "SELECT * FROM rate WHERE symbol = '$code'")->fetch_assoc();
    if ($exist) {
      mysqli_query($this->getDb(), "UPDATE rate SET rate='$rate' WHERE symbol = '$code'");
    } else {
      mysqli_query($this->getDb(), "INSERT INTO rate (symbol, rate) VALUES ('$code','$rate')");
    }
  }

  /**
   * Echo records from db
   */
  public function show() {
    $r = mysqli_query($this->getDb(), "SELECT * FROM rate LIMIT 10");
    while ($res = mysqli_fetch_assoc($r)) {
      echo $res['symbol'] . ' ' . $res['rate'] . ' ' . PHP_EOL;
    }
  }

  /**
   * Delete record by symbol
   *
   * @param $code
   */
  public function delete($code) {
    mysqli_query($this->getDb(), "DELETE FROM rate WHERE symbol = '$code' ");
  }

  /**
   * import data from remote source
   */
  private function importRemoteData() {
    $content = file_get_contents(__DIR__ . '/rates2.json');
    $data = json_decode($content, true);
    $count = 0;
    foreach ($data as $info) {
      foreach ($info as $code => $rate) {
        $this->update($code, $rate);
        $count++;
      }
    }
    return "Records loaded: " . $count . PHP_EOL;
  }

  /**
   * import data from local file
   */
  private function importLocalData() {
    $content = file_get_contents($this->domain . "/rates1.json");
    $data = json_decode($content);
    $count = 0;
    foreach ($data->rates as $info) {
      $this->update($info->symbol, $info->rate);
      $count++;
    }
    return "Records loaded: " . $count . PHP_EOL;
  }

  /**
   * Processing request
   *
   * @param int $source
   */
  public function process($source = self::SOURCE_LOCAL) {
    if ($source == self::SOURCE_WEB) {
      echo $this->importRemoteData();
    } else if ($source == self::SOURCE_LOCAL) {
      echo $this->importLocalData();
    } else {
      echo "Error occurred" . PHP_EOL;
    }
  }
} 
