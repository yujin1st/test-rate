<?php
include "Import.php";

class ImportTest extends \PHPUnit_Framework_TestCase
{
  /** non existent index */
  const NON_EXISTENT = 'NON';

  public function testDelete() {
    $import = new Import();
    $db = $import->getDb();

    mysqli_query($db, "INSERT INTO rate (symbol, rate) VALUES ('" . self::NON_EXISTENT . "','0')");
    $import->delete(self::NON_EXISTENT);
    $res = mysqli_query($db, "SELECT * FROM rate WHERE symbol = '" . self::NON_EXISTENT . "'")->fetch_assoc();
    $exist = $res ? 1 : 0;
    $this->assertEquals(0, $exist);
  }

  /**
   * updating record
   */
  public function testUpdate() {
    $import = new Import();
    $db = $import->getDb();

    $import->update(self::NON_EXISTENT, '123');
    $import->update(self::NON_EXISTENT, '321');
    $res = mysqli_query($db, "SELECT * FROM rate WHERE symbol = '" . self::NON_EXISTENT . "'")->fetch_assoc();
    $this->assertEquals(321, $res['rate']);

    $import->delete(self::NON_EXISTENT);
  }

  /**
   * insert nonexistent record
   */
  public function testInsert() {
    $import = new Import();
    $db = $import->getDb();

    $import->update(self::NON_EXISTENT, '123');
    $res = mysqli_query($db, "SELECT * FROM rate WHERE symbol = '" . self::NON_EXISTENT . "'")->fetch_assoc();
    $this->assertEquals($res['rate'], 123);

    $import->delete(self::NON_EXISTENT);
  }

}
