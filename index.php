<?php
/**
 * User: yujin
 * Date: 30.03.16
 * Time: 17:11
 */
require "Import.php";

$import = new Import();
$type = defined('STDIN') ? $argv[1] : (isset($_GET['type']) ? $_GET['type'] : Import::SOURCE_LOCAL);
$import->process($type);
$import->show();

